//libraries

#include <stdio.h>
#include "hardware/adc.h"
#include "hardware/vreg.h"
#include "hardware/dma.h"
#include "hardware/pll.h"
#include "hardware/clocks.h"
#include "math.h"
#include "pico/multicore.h"
#include "pico/util/datetime.h"
#include "Adafruit_ZeroFFT.h"
#include "arduino.h"
#include <WiFi.h>
#include <WebServer.h>

//frontend
  // Set up WiFi credentials
  const char* ssid = "GreedyInternetProvider";
  const char* password = "manamana";



//MIDI structure
  int note_midi[85]={43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127}; 
  float note_freq[85]={97.998859,103.826174,110.000000,116.540940,123.470825,130.812783,138.591315,146.832384,155.563492,164.813778,174.614116,184.997211,195.997718,207.652349,220.000000,233.081881,246.941651,261.625565,277.182631,293.664768,311.126984,329.627557,349.228231,369.994423,
                          391.995436,415.304698,440.000000,466.163762,493.883301,523.251131,554.365262,587.329536,622.253967,659.255114,698.456463,739.988845,783.990872,830.609395,880.000000,932.327523,987.766603,1046.502261,1108.730524,1174.659072,1244.507935,1318.510228,1396.912926,1479.977691,
                          1567.981744,1661.218790,1760.000000,1864.655046,1975.533205,2093.004522,2217.461048,2349.318143,2489.015870,2637.020455,2793.825851,2959.955382,3135.963488,3322.437581,3520.000000,3729.310092,3951.066410,4186.009045,4434.922096,4698.636287,4978.031740,5274.040911,5587.651703,
                          5919.910763,6271.926976,6644.875161,7040.000000,7458.620184,7902.132820,8372.018090,8869.844191,9397.272573,9956.063479,10548.081821,11175.303406,11839.82,12543.85}; 






//defines
  #define NFFT 4096
  #define SAMPLES 2048
  #define DC_VAL 2500
  #define CAPTURE_CHANNEL 0
  #define CLOCKDIVIDER 1600
  #define WINDOW 20 
  #define LOW_BINS 10 //determines how many FFT values near zero are to be set to zero
  #define FFT_FLOOR 10 //determines what os considere a random noise and what is acrually useful
//variables
  float BIN_SIZE = 11;
  int16_t read_buffer[NFFT];
  int16_t transfer_buffer[NFFT/2];
  //variables
  uint64_t read_time2;
  uint64_t read_time;  //total time of sample aquisition
  float FS=30000;         //sampling freq
  float FREQ=0;
  int flipflop = 0;
  int freq_avg = 0;
  int DC=0;


//backend
    void info_print() {
      Serial.println(" ");
      Serial.print("read time:");
      Serial.println(read_time);
      Serial.println(FS);
      Serial.print("total_time:");
      Serial.println(micros() - read_time2);
      BIN_SIZE=FS/NFFT;
      Serial.print("frequency step:");
      Serial.println(BIN_SIZE);
    }
    float find_max(){
      int max_val=FFT_FLOOR;
      int max_index=0;
      float out_index=0;
      for(int i=0;i<LOW_BINS;i++)
      {
        read_buffer[i]=3;
      }
      for(int i=LOW_BINS;i<NFFT/2;i++){
        if(read_buffer[i]>max_val){
          max_val=read_buffer[i];
          max_index=i;
        }
      }
      
      if(max_index>LOW_BINS && max_val>FFT_FLOOR){
        return max_index;
      }
    
      return 0;
    }
    void data_print(){
      for (int i = 0; i < NFFT/2; i++) {
        Serial.println(read_buffer[i]);
      }

    }
    int freq_to_midi(){
      int midi=0;
      if(FREQ>100 && FREQ<12000){
        for(int i=0;i<82;i++){
          if(note_freq[i]<FREQ&&FREQ<note_freq[i+1]){
            midi=  i+43;
            break;
            if(FREQ-note_freq[i]>note_freq[i+1]-FREQ){
              midi= i;
              break;
            }
            else{
              midi= (i+1);
              break;
            }
          }
        }
      }
      else{
        midi= -1;
      }
      return midi;
    }
    void send_MIDI(byte noteOnStatus, byte noteNumber, byte velocity) {
      Serial1.write(noteOnStatus);
      Serial1.write(noteNumber);
      // Check if the MIDI message is a two-byte message (e.g., Note On, Note Off)
      if ((noteOnStatus & 0xF0) != 0xC0 && (noteOnStatus & 0xF0) != 0xD0) {
        Serial1.write(velocity);
      }
  }

  // Set up global variable
  int mode_select = 0;
  int midi =10;

  WebServer server(80);

  //POST request handler
  void handlePost(){
    if (server.method() == HTTP_POST) {
      String valueStr = server.arg("mode");
      if (valueStr == "1" || valueStr == "0") {
        mode_select = valueStr.toInt();
        server.send(200, "text/plain", "Value set successfully");
      } else {
        server.send(400, "text/plain", "Invalid value. It should be either 1 or 0.");
      }
    } else {
      server.send(405, "text/plain", "Method Not Allowed");
    }
  }

  //first outcome handler (musical note)
  void handleOutcome1() {
      server.send(200, "text/plain", String(midi));
  }
  //second outcome handler (Spectrum note)
  void handleOutcome2() {
    int dataArray[NFFT/2];
    String dataString = "";
      for (int i = 0; i <NFFT/2; i++){
        dataString += transfer_buffer[i];
        if (i < sizeof(dataArray) / sizeof(dataArray[0]) - 1) {
          dataString += ","; 
        }
      }
      server.send(200, "text/plain", dataString);
  }



/////////////////////////////////////////////////////Core0
void setup() {
  Serial.begin(115200);
  delay(1000);

  // Connect to WiFi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }

  Serial.println("Connected to WiFi");
  // Print IP address
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
  // Set up routes
  server.on("/select", handlePost);
  server.on("/note", handleOutcome1);
  server.on("/spectrum", handleOutcome2);

  // Start server
  server.begin();
  Serial.println("HTTP server started");
}
void loop() {
  if(rp2040.fifo.pop_nb(FREQ)){
    DC=rp2040.fifo.pop();
    DC=(DC*120/4096);
  }
  
  if(mode_select==0){
    rp2040.fifo.push(0);
  }
  if(mode_select==1){
    rp2040.fifo.push(1);
    for(int i=0;i<NFFT/2;i++){
      transfer_buffer[i]=rp2040.fifo.pop();
    }
  }
  midi=freq_to_midi();
  if(midi>0){
    if(DC > 0){
      send_MIDI(10010000, midi, DC);
    }
    else{
      send_MIDI(10000000, midi, DC);
    }
  }
  
  
  server.handleClient();
}
/////////////////////////////////////////////core1
void setup1(){
  Serial.begin(115200);
  adc_gpio_init(26 + CAPTURE_CHANNEL);
  adc_init();
  adc_select_input(CAPTURE_CHANNEL);
  adc_fifo_setup(true, false, 0, false, false);
  adc_set_clkdiv(CLOCKDIVIDER);
}
void loop1(){
  float local_freq=0;
  adc_run(true);  //run adc
  read_time2 = micros();
  for (int i = 0; i < SAMPLES; i++) {
    read_buffer[i] = adc_fifo_get_blocking()-DC_VAL;  //gets last sample from adc
  }
  
  adc_run(false);  //stops adc
  for (int i = SAMPLES; i < NFFT; i++){//zero paddind
  read_buffer[i]=0;
  }
  
  ZeroFFT(read_buffer, NFFT);
  rp2040.fifo.push(find_max()*FS/NFFT);
  //volume read
  adc_select_input(1);
  rp2040.fifo.push(adc_read());
  adc_select_input(0);
  
  if(rp2040.fifo.pop()==1){
      for (int i = 0; i <NFFT/2; i++){
        rp2040.fifo.push(read_buffer[i]);
      }
    }
  read_time = micros() - read_time2;
  for(int i=0;i<LOW_BINS;i++){//sets values near dc to zero
    read_buffer[i]=0;
  }
}
