from flask import Flask, render_template, request, jsonify
import requests

import numpy as np
import random

app = Flask(__name__)

# Pico server IP address
pico_ip = None

# Function to update Pico mode
def update_mode(mode):
    global pico_ip
    print("mode:")
    try:
        if mode == 1:
            mode_string = "1"
        elif mode == 0:
            mode_string = "0"
        else:
            raise ValueError("Invalid mode. Mode must be either 1 or 0.")
        
        response = requests.post(f"http://{pico_ip}/select", data={'mode': mode_string})
        if response.status_code == 200:
            print("Changing Pico mode successful.")
        else:
            print("Error changing Pico mode. Status code:", response.status_code)


        requests.post(f"http://{pico_ip}/select", data={'mode': mode})
        print("changing pico mode")
    except requests.exceptions.RequestException as e:
        print("Error updating Pico mode:", e)

# Function to fetch data for plotting from Pico server handleOutcome1
def fetch_data1():
    global pico_ip
    if pico_ip:
        try:
            response = requests.get(f"http://{pico_ip}/note")
            data = response.text
            return data
        except requests.exceptions.RequestException as e:
            print("Error fetching data:", e)
            return "Error fetching data: " + str(e)
    else:
        print("Pico IP not set or invalid")
        return "Pico IP not set or invalid"

# Function to fetch data for plotting from Pico server handleOutcome2
def fetch_data2():
    global pico_ip
    if pico_ip:
        try:
            response = requests.get(f"http://{pico_ip}/spectrum")
            data = str(response.text)
            print(data)
            return data
        except requests.exceptions.RequestException as e:
            print("Error fetching data:", e)
    return "No data available for page 1"
    #return array_data
    

# Index route
@app.route('/')
def index():
    return render_template('index.html', pico_ip=pico_ip)

# Page 1 route
@app.route('/page1')
def page1():
    update_mode(0)
    data = fetch_data1()
    return render_template('page1.html', pico_ip=pico_ip, data=data)

# Page 2 route
@app.route('/page2')
def page2():
    update_mode(1)
    data = fetch_data2()
    
    return render_template('page2.html', pico_ip=pico_ip, data=data)

# Route for fetching data for page 1
@app.route('/page1_data')
def page1_data():
    data = fetch_data1()
    return jsonify(data)

# Route for fetching data for page 2
@app.route('/page2_data')
def page2_data():
    data = fetch_data2()
    
    return data

# Connect route
@app.route('/connect', methods=['POST'])
def connect():
    global pico_ip
    pico_ip = request.form['pico_ip']
    return '', 204


if __name__ == '__main__':
    app.run(debug=True)